# Copyright 2022 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
---

- name: create prometheus system group
  group:
    name: prometheus
    system: true
    state: present
  become: true

- name: create prometheus system user
  user:
    name: prometheus
    system: true
    shell: "/usr/sbin/nologin"
    group: prometheus
    createhome: false
  become: true

- name: make sure /etc/prometheus/ssl exists
  file:
    path: "/etc/prometheus/ssl/{{ item }}"
    state: directory
    owner: prometheus
    group: prometheus
    mode: 0700
  become: true
  with_items:
    - "cert"
    - "private"
    
- name: make sure /etc/prometheus/jupyterhub exists
  file:
    path: "/etc/prometheus/jupyterhub/{{ item }}"
    state: directory
    owner: prometheus
    group: prometheus
    mode: 0700
  become: true
  with_items:
    - "{{ groups['jupyterhub'] }}"
  
- name: install TLS key
  copy:
    src: "{{ local_tls_dir }}/{{ inventory_hostname }}/{{ inventory_hostname }}.key"
    dest: "/etc/prometheus/ssl/private/{{ inventory_hostname }}.key"
    owner: prometheus
    group: prometheus
    mode: 0600
  become: true

- name: install TLS certificate
  copy:
    src: "{{ local_tls_dir }}/{{ inventory_hostname }}/{{ inventory_hostname }}.crt"
    dest: "/etc/prometheus/ssl/cert/{{ inventory_hostname }}.crt"
    owner: prometheus
    group: prometheus
    mode: 0600
  become: true

- name: copy jupyterhub internal-ssl certs to prometheus host
  block:
    - name: make sure /tmp/jupyterhub directory exists
      file:
        path: "/tmp/{{ item }}"
        state: directory
      delegate_to: localhost
      with_items:
        - "{{ groups['jupyterhub'] }}"
      
    - name: copy certs from jupyterhub host to ansible controller
      fetch:
        src: "{{ hostvars[inventory_hostname]['jupyterhub_internal_certs_dir'] }}/{{ item[1] }}"
        dest: "/tmp/{{ item[0] }}/"
        flat: yes
      delegate_to: "{{ item[0] }}"
      with_nested:
        - "{{ groups['jupyterhub'] }}"
        - ["hub-ca_trust.crt", "proxy-client/proxy-client.key", "proxy-client/proxy-client.crt"]
        
    - name: copy certs from ansible controller to prometheus host
      copy:
        src: "/tmp/{{ item }}/"
        dest: "/etc/prometheus/jupyterhub/{{ item }}"
        owner: prometheus
        group: prometheus
        mode: 0600
      with_items:
        - "{{ groups['jupyterhub'] }}"
    
    - name: copy jupyterhub metrics token to prometheus host
      copy:
        src: "{{ local_tokens_dir }}/metrics_token"
        dest: "/etc/prometheus/jupyterhub/{{ item }}"
      with_items:
        - "{{ groups['jupyterhub'] }}"
        
  always:  
    - name: delete certs on ansible controller
      file:
        name: "/tmp/{{ item }}"
        state: absent
      delegate_to: localhost
      with_items:
        - "{{ groups['jupyterhub'] }}"
  tags:
    - molecule-idempotence-notest
    
- name: check if prometheus scrape config already exists
  stat:
    path: "/etc/prometheus/prometheus.yml"
  become: true
  register: prometheus_scrape_config
    
- name: read the existing prometheus scrape config
  block:
    - name: slurp prometheus config file
      slurp:
        src: '/etc/prometheus/prometheus.yml'
      register: slurpfile
      
    - name: set fact for existing prometheus config file
      set_fact:
        _existing_prometheus_scrape_configs: "{{ slurpfile['content'] | b64decode  | from_yaml }}"
  become: true
  when: prometheus_scrape_config.stat.exists
  
# Technically we should use hostvars[item]['node_exporter_address'] to get node exporter address
# and port. Here item is name of the node exporter node. But this might fail when we explictly
# use tags, for instance --tag "prometheus". In this case role corresponding to node exporter is 
# not executed and hence there are no hostvars for the node exporter host. We define all the important
# global variables in the config.yml and hence we can access them from any hostvars. That is why
# we get them from prometheus hostvars and they will be exactly same as node exporter hostvars
- name: generate prometheus scrape configuration for self signed certs
  set_fact:
    _generated_prometheus_scrape_configs: "{{ _generated_prometheus_scrape_configs | default([]) + [{ 'job_name': item + '_node_exporter', 'metrics_path': '/metrics', 'scheme': 'https', 'basic_auth': { 'username': 'node_exporter', 'password': lookup('file', local_passwds_dir + '/' + item + '_node_exporter_passwd') | trim }, 'tls_config': {'ca_file': '/etc/ssl/ca/jupyterhub-ca.crt', 'insecure_skip_verify': True }, 'static_configs': [{ 'targets': [hostvars[inventory_hostname]['node_exporter_address'] + ':' + hostvars[inventory_hostname]['node_exporter_port']] }] }] + [{ 'job_name': item + '_jupyterhub', 'metrics_path': '/metrics', 'scheme': 'https', 'authorization': { 'credentials_file': '/etc/prometheus/jupyterhub/' + item + '/metrics_token' }, 'tls_config': { 'ca_file': '/etc/prometheus/jupyterhub/' + item + '/hub-ca_trust.crt', 'cert_file': '/etc/prometheus/jupyterhub/' + item + '/proxy-client.crt', 'key_file': '/etc/prometheus/jupyterhub/' + item + '/proxy-client.key', 'insecure_skip_verify': True }, 'static_configs': [{ 'targets': [item + ':8081'] }] }] }}"
  with_items:
    - "{{ groups['node_exporter'] }}"
  when: self_signed_certs
  
- name: generate prometheus scrape configuration for external CA signed certs
  set_fact:
    _generated_prometheus_scrape_configs: "{{ _generated_prometheus_scrape_configs | default([]) + [{ 'job_name': item + '_node_exporter', 'metrics_path': '/metrics', 'scheme': 'https', 'basic_auth': { 'username': 'node_exporter', 'password': lookup('file', local_passwds_dir + '/' + item + '_node_exporter_passwd') | trim }, 'static_configs': [{ 'targets': [hostvars[inventory_hostname]['node_exporter_address'] + ':' + hostvars[inventory_hostname]['node_exporter_port']] }] }] + [{ 'job_name': item + '_jupyterhub', 'metrics_path': '/metrics', 'scheme': 'https', 'authorization': { 'credentials_file': '/etc/prometheus/jupyterhub/' + item + '/metrics_token' }, 'tls_config': { 'ca_file': '/etc/prometheus/jupyterhub/' + item + '/hub-ca_trust.crt', 'cert_file': '/etc/prometheus/jupyterhub/' + item + '/proxy-client.crt', 'key_file': '/etc/prometheus/jupyterhub/' + item + '/proxy-client.key', 'insecure_skip_verify': True }, 'static_configs': [{ 'targets': [item + ':8081'] }] }] }}"
  with_items:
    - "{{ groups['node_exporter'] }}"
  when: not self_signed_certs
  
- name: get resultant prometheus config from existing one and generated one
  set_fact:
    prometheus_scrape_configs: "{{ _generated_prometheus_scrape_configs | merge_prometheus_config(_existing_prometheus_scrape_configs | default({})) }}"
