# Preflights

This role contains tasks like generating tokens, TLS certificates, *etc* needed
for JupyterHub installation.
